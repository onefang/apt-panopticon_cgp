<?php

require_once 'config.php';

if (!ini_get('date.timezone')) {
	date_default_timezone_set($CONFIG['default_timezone']);
}
