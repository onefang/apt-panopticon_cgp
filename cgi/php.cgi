#!/bin/bash

# Wrap PHP for web servers that can only do PHP via CGI.
export DOCUMENT_ROOT=$(pwd)
export SCRIPT_FILENAME="$PATH_TRANSLATED"
export SCRIPT_NAME=$(basename $PATH_TRANSLATED)
exec /usr/bin/php-cgi --php-ini ${DOCUMENT_ROOT}/php.ini
